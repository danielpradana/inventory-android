package id.project.feature

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.project.R
import id.project.core.BaseActivity
import id.project.core.Inventory
import id.project.core.toActivity
import kotlinx.android.synthetic.main.activity_inventory.*
import kotlinx.android.synthetic.main.item_inventory.tv_title
import kotlinx.android.synthetic.main.item_inventory.view.*
import kotlinx.android.synthetic.main.layout_toolbar_inventory.*

class InventoryActivity : BaseActivity() {
    private val dataInventory = ArrayList<Inventory>()

    override fun setLayout(): Int = R.layout.activity_inventory

    override fun setUpVariable() {
        initView()
        initData()
        initAdapter()
    }

    private fun initView(){
        tv_title.text = "Inventory"
        ivBack.setOnClickListener {
            finish()
        }

        iv_add.setOnClickListener {
            toActivity(null, AddInventoryActivity::class.java)
        }
    }

    private fun initData(){
        dataInventory.add(Inventory("Battery Aki", 5))
        dataInventory.add(Inventory("Kampas Rem", 4))
        dataInventory.add(Inventory("Rantai Set", 10))
        dataInventory.add(Inventory("Saringan Udara", 9))
        dataInventory.add(Inventory("Busi", 9))
        dataInventory.add(Inventory("Ban Luar", 7))
        dataInventory.add(Inventory("CVT Grease KIT", 5))
        dataInventory.add(Inventory("Oil Filter", 2))
    }

    private fun initAdapter(){
        rv_inventory.layoutManager = LinearLayoutManager(this)
        rv_inventory.adapter = Adapter(this, dataInventory)
    }

    override fun internetAvailable() {
        TODO("Not yet implemented")
    }

    override fun internetUnAvailable() {
        TODO("Not yet implemented")
    }

    inner class Adapter(val context: Context, val list: ArrayList<Inventory>) : RecyclerView.Adapter<AdapterVH>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterVH {
            return AdapterVH(LayoutInflater.from(context).inflate(R.layout.item_inventory, parent, false))
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(holder: AdapterVH, position: Int) {
            holder.setIsRecyclable(false)
            val item = list[position]
            var qty: Int = item.qty

            holder.tvTitle?.text = item.itemName
            holder.tvQty?.text = qty.toString()

            holder.cvPlus?.setOnClickListener {
                qty++
                dataInventory.set(position, Inventory(item.itemName, qty))
                notifyDataSetChanged()
            }

            holder.cvMinus?.setOnClickListener {
                qty--
                dataInventory.set(position, Inventory(item.itemName, qty))
                notifyDataSetChanged()
            }
        }

    }

    class AdapterVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle: TextView? = itemView.tv_title
        val cvPlus: CardView? = itemView.cv_plus
        val cvMinus: CardView? = itemView.cv_minus
        val tvQty: TextView? = itemView.tv_qty
    }
}