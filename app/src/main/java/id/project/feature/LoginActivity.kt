package id.project.feature

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.project.R
import id.project.core.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    override fun setLayout(): Int = R.layout.activity_login

    override fun setUpVariable() {
        cv_login.setOnClickListener {
            if(et_phone.text.toString().length > 10){
                toActivity(null, PinActivity::class.java)
            }
        }
    }

    override fun internetAvailable() {
    }

    override fun internetUnAvailable() {

    }
}