package id.project.feature

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import id.project.R
import id.project.core.BaseActivity
import id.project.core.ScanActivity
import id.project.core.ShowImageQR
import kotlinx.android.synthetic.main.activity_add_inventory.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class AddInventoryActivity : BaseActivity() {
    private var showImageQr: ShowImageQR? = null
    private val requestCode: Int = 100

    override fun setLayout(): Int = R.layout.activity_add_inventory

    override fun setUpVariable() {
        showImageQr = ShowImageQR(this)
        tv_title.text = "Insert Stock"
        ivBack.setOnClickListener {
            finish()
        }
        cv_insert.setOnClickListener {
            finish()
        }
        cv_qr.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), requestCode)
            }else {
                startActivityForResult(Intent(this, ScanActivity::class.java), 21)
            }
        }
    }

    override fun internetAvailable() {

    }

    override fun internetUnAvailable() {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            val str = data.getStringExtra("data")
            str?.let { }

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == this.requestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

}