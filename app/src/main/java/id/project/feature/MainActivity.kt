package id.project.feature

import id.project.R
import id.project.core.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun setLayout(): Int = R.layout.activity_main

    override fun setUpVariable() {
        cl_greeting.setOnClickListener {
            toActivity(null, ProfileActivity::class.java)
        }

        cv_inventory.setOnClickListener {
            toActivity(null, InventoryActivity::class.java)
        }

        cv_history.setOnClickListener {
            toActivity(null, HistoryActivity::class.java)
        }

        iv_logout.setOnClickListener {
            initLogout()
        }

    }

    private fun initLogout(){
        val baseDialog = BaseDialog(this)
        baseDialog.createDialog("Konfirmasi", "Apakah Anda yakin ingin keluar?",
            "Ya",
            "Tidak", object : BaseDialog.Action {
                override fun onActionNo() {

                }

                override fun onAction() {
                    saveBooleanPref(Constants.is_login, false)
                    toActivity(null, LoginActivity::class.java)
                    finish()
                }
            })
    }

    override fun internetAvailable() {
    }

    override fun internetUnAvailable() {

    }
}