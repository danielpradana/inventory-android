package id.project.feature

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import id.project.R
import id.project.core.BaseActivity
import id.project.core.Constants
import id.project.core.getBooleanPref
import id.project.core.toActivity

class SplashActivity : BaseActivity() {

    override fun setLayout(): Int = R.layout.activity_splash

    override fun setUpVariable() {
        val handler = Handler()
        handler.postDelayed({
            if (getBooleanPref(Constants.is_login))
                toActivity(null, MainActivity::class.java)
            else
                toActivity(null, LoginActivity::class.java)
            finish()
        }, 2000)
    }

    override fun internetAvailable() {

    }

    override fun internetUnAvailable() {

    }
}