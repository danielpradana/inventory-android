package id.project.feature

import id.project.R
import id.project.core.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.layout_toolbar_profile.*

class ProfileActivity : BaseActivity() {

    override fun setLayout(): Int = R.layout.activity_profile

    override fun setUpVariable() {
        initData()
        ivBack.setOnClickListener {
            finish()
        }
        ivSave.setOnClickListener {
            val baseDialog = BaseDialog(this)
            baseDialog.createDialog("Konfirmasi", "Apakah Anda yakin ingin mengubah data profile?",
                "Ya",
                "Tidak", object : BaseDialog.Action {
                    override fun onActionNo() {

                    }

                    override fun onAction() {
                        showDialogOk("", "Data berhasil diubah")
                    }
                })
        }
    }

    private fun initData(){
        et_nama.setText("Admin")
        et_email.setText("admin@yasuka.id")
        et_phone.setText("0811223344")
        et_birthday.setText("08 Januari 1993")
    }

    override fun internetAvailable() {

    }

    override fun internetUnAvailable() {

    }
}