package id.project.feature

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import id.project.R
import id.project.core.*
import kotlinx.android.synthetic.main.activity_pin.*

class PinActivity : BaseActivity() {
    private var indexDot: Int = 0
    private var pinValue: String = ""

    val pinActive by lazy {
        arrayOf(
            pin1_active, pin2_active, pin3_active, pin4_active
        )
    }

    override fun setLayout(): Int = R.layout.activity_pin

    override fun setUpVariable() {
        initView()
    }

    private fun initView(){
        iv_back.setOnClickListener {
            finish()
        }

        et_pin.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(key: CharSequence, start: Int, before: Int, count: Int) {
                val pin = key.toString()

                if (pin.isNotEmpty()) {
                    if (pin.length <= 4) {
                        pinValue = key.toString()
                    }
                }else{
                    if (pinValue.isNotEmpty())
                        pinValue = pinValue.substring(0, pinValue.length - 1)
                }
                updateDots()
            }
        })

        cv_login.setOnClickListener {
            if (pinValue.length == 4){
                saveBooleanPref(Constants.is_login, true)
                toActivity(null, MainActivity::class.java)
                finish()
            }else{
                Toast.makeText(this, "PIN belum lengkap", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun updateDots() {
        if (pinValue.length > indexDot) {
            pinActive[indexDot].visible()
            indexDot++
        } else if (pinValue.length < indexDot){
            if (indexDot in 1..4) {
                indexDot--
                pinActive[indexDot].gone()
            }
        }
    }

    override fun internetAvailable() {

    }

    override fun internetUnAvailable() {

    }
}