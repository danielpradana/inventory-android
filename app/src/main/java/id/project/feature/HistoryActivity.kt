package id.project.feature

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.project.R
import id.project.core.BaseActivity
import id.project.core.History
import id.project.core.Inventory
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.item_history.view.*
import kotlinx.android.synthetic.main.item_history.view.tv_title
import kotlinx.android.synthetic.main.item_inventory.tv_title
import kotlinx.android.synthetic.main.layout_toolbar.*

class HistoryActivity : BaseActivity() {
    private val dataHistory = ArrayList<History>()

    override fun setLayout(): Int = R.layout.activity_history

    override fun setUpVariable() {
        initView()
        initData()
        initAdapter()
    }

    private fun initView(){
        tv_title.text = "History"
        ivBack.setOnClickListener {
            finish()
        }
    }

    private fun initData(){
        dataHistory.add(History(0, "Battery Aki", "3", "2"))
        dataHistory.add(History(1, "Kampas Rem", "4", "3"))
        dataHistory.add(History(2, "Rantai Set", "6", "5"))
        dataHistory.add(History(0, "Saringan Udara", "2", "2"))
        dataHistory.add(History(1, "Busi", "1", "2"))
        dataHistory.add(History(2, "Ban Luar", "4", "5"))
        dataHistory.add(History(0, "CVT Grease KIT", "5", "1"))
        dataHistory.add(History(1, "Oil Filter", "4", "2"))
    }

    private fun initAdapter(){
        rv_history.layoutManager = LinearLayoutManager(this)
        rv_history.adapter = Adapter(this, dataHistory)
    }

    override fun internetAvailable() {
        TODO("Not yet implemented")
    }

    override fun internetUnAvailable() {
        TODO("Not yet implemented")
    }

    inner class Adapter(val context: Context, val list: ArrayList<History>) : RecyclerView.Adapter<AdapterVH>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterVH {
            return AdapterVH(LayoutInflater.from(context).inflate(R.layout.item_history, parent, false))
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(holder: AdapterVH, position: Int) {
            holder.setIsRecyclable(false)
            val item = list[position]

            holder.tvTitle?.text = item.productTitle
            holder.tvIn?.text = item.productIn
            holder.tvOut?.text = item.productOut
        }

    }

    class AdapterVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle: TextView? = itemView.tv_title
        val tvIn: TextView? = itemView.tv_in
        val tvOut: TextView? = itemView.tv_out
    }

}