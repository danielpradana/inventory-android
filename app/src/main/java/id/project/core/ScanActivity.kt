package id.project.core

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView



class ScanActivity : AppCompatActivity(), ZXingScannerView.ResultHandler{

    private var mScannerView: ZXingScannerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mScannerView =  ZXingScannerView(this)  // Programmatically initialize the scanner view
        setContentView(mScannerView)
    }

    override fun handleResult(rawResult: Result?) {
        if(rawResult != null && rawResult.text.isNotEmpty()){
            setResult(Activity.RESULT_OK, Intent().apply { putExtra("data", rawResult?.text) })
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        mScannerView?.setResultHandler(this)// Register ourselves as a handler for scan results.
        mScannerView?.startCamera()
    }

    override fun onPause() {
        super.onPause()
        mScannerView?.stopCamera()
    }

    override fun onStop() {
        super.onStop()
        mScannerView?.stopCamera()
    }

}