package id.project.core

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

fun Context.savePref(key: String, value: String) {
    val sharedPref = this.getSharedPreferences(Constants.my_pref, Context.MODE_PRIVATE)
    sharedPref.edit().putString(key, value).apply()
}

fun Context.getPref(key: String): String? {
    this.getSharedPreferences(Constants.my_pref, Context.MODE_PRIVATE).apply {
        return getString(key, "")
    }
}

fun Context.saveBooleanPref(key: String, value: Boolean) {
    val sharedPref = this.getSharedPreferences(Constants.my_pref, Context.MODE_PRIVATE)
    sharedPref.edit().putBoolean(key, value).apply()
}

fun Context.getBooleanPref(key: String): Boolean {
    this.getSharedPreferences(Constants.my_pref, Context.MODE_PRIVATE).apply {
        return getBoolean(key, false)
    }

}

fun <T> AppCompatActivity.toActivity(
    bundle: Bundle? = null, destination: Class<T>
) {
    val intent = Intent(baseContext, destination)
    if (bundle != null) intent.putExtras(bundle)
    startActivity(intent)
}

fun ImageView.loadImage(url: String) {
    Glide.with(this).load(url).apply(
        RequestOptions().diskCacheStrategy(
            DiskCacheStrategy.ALL)).into(this)
}

//  is network connected
fun Context.isNetworkConnected(): Boolean {
    val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    val activeNet: NetworkInfo?
    return if (cm != null) {
        activeNet = cm.activeNetworkInfo
        activeNet != null && activeNet.isConnected
    } else
        false
}

fun AppCompatActivity.showDialogOk(title: String, message: String){
    val baseDialog = BaseDialog(this)
    baseDialog.createDialog(title, message,
        "OK",
        "", object : BaseDialog.Action {
            override fun onActionNo() {

            }

            override fun onAction() {

            }
        })
}

fun View.visible(){
    visibility = View.VISIBLE
}

fun View.gone(){
    visibility = View.GONE
}

fun View.invisible(){
    visibility = View.INVISIBLE
}
