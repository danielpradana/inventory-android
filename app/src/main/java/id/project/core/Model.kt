package id.project.core

data class Inventory(
    val itemName: String,
    val qty: Int
)

data class History(
    val productId: Int,
    val productTitle: String,
    val productIn: String,
    val productOut: String
)