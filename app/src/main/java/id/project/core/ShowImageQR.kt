package id.project.core

import android.app.Dialog
import android.content.Context
import android.view.WindowManager
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import id.project.R
import kotlinx.android.synthetic.main.image_qr.*

class ShowImageQR(context: Context) : Dialog(context) {
    var valueQr: String? = null
    var onDismiss : ((isDismiss: Boolean) -> Unit)? = null

    init {
        window?.setBackgroundDrawableResource(android.R.color.transparent)

        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT)

        setCancelable(true)
        setContentView(R.layout.image_qr)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        valueQr?.let {
            val multiFormatWriter = MultiFormatWriter()
            try
            {
                val bitMatrix = multiFormatWriter.encode(it, BarcodeFormat.QR_CODE, 233, 233)
                val barcodeEncoder = BarcodeEncoder()
                val bitmap = barcodeEncoder.createBitmap(bitMatrix)
                iv_qr.setImageBitmap(bitmap)
            }
            catch (e: WriterException) {
                e.printStackTrace()
            }
        }
    }

    override fun dismiss() {
        super.dismiss()
        onDismiss?.let {
            it(true)
        }
    }


}