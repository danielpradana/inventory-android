package id.project.core

import android.annotation.SuppressLint
import androidx.appcompat.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import id.project.R


class BaseDialog(val context: Context) {

    private var tvTitle: TextView? = null
    private var tvMessage: TextView? = null
    var isShow: Boolean = false

    @SuppressLint("InflateParams")
    fun createDialog(title: String?, message: String, positiveButton: String,
                     negativeButton: String?, action: Action, center:Boolean = false) {
        val builder = AlertDialog.Builder(context)
        builder.setCancelable(false)
        val view = LayoutInflater.from(context).inflate(R.layout.base_dialog_layout, null, false)
        tvMessage = view.findViewById(R.id.tv_message)
        tvTitle = view.findViewById(R.id.tv_title)
        if (center) {
            tvTitle?.setTypeface(tvTitle?.typeface, Typeface.BOLD)
            val param = ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.WRAP_CONTENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT
            )
            param.topToTop = ConstraintSet.PARENT_ID
//            param.leftToLeft = ConstraintSet.PARENT_ID
//            param.setMargins(16.toPx(), 16.toPx(), 0, 8.toPx())
            tvTitle?.layoutParams = param
        }
        val btnPositive = view.findViewById<TextView>(R.id.btn_positive)
        val btnNegative = view.findViewById<TextView>(R.id.btn_negative)
        builder.setView(view)
        val dialog = builder.create()

        tvMessage?.text = message

        if (!title.isNullOrEmpty()) {
            tvTitle?.text = title
            tvTitle?.visibility = View.VISIBLE
        } else {
            tvTitle?.visibility = View.GONE
        }

        btnPositive.text = positiveButton
        btnPositive.setOnClickListener {
            action.onAction()
            isShow = false
            dialog.dismiss()
        }

        if (negativeButton != null)
            btnNegative.text = negativeButton
        btnNegative.setOnClickListener {
            action.onActionNo()
            isShow = false
            dialog.dismiss()
        }

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        if (!isShow){
            dialog.show()
            isShow = true
        }
    }


    interface Action {
        fun onAction()
        fun onActionNo()
    }

}